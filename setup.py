# Copyright (c) 2017, Synopsys, Inc. All rights reserved.
# License: MIT
"""Setup module for Protecode SC command line tool."""

from codecs import open
from os import path
from setuptools import setup, find_packages

from protecodesc import __version__ as version


here = path.abspath(path.dirname(__file__))
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='protecodesc',
    version=version,
    description="Protecode SC command line tools and API client",
    long_description=long_description,
    url='',
    author='Antti Hayrynen',
    author_email='hayrynen@synopsys.com',
    license='MIT',
    packages=find_packages(exclude=['tests']),
    zip_safe=False,
    install_requires=['click', 'requests', 'keyring'],
    entry_points={
        'console_scripts': [
            'protecodesc = protecodesc.cli:main'
        ]
    },
    classifiers=[
        "Development Status :: 4 - Beta",
        "Environment :: Console",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.5",
        "Topic :: Security",
        "Topic :: Software Development",
        "Topic :: Software Development :: Libraries",
        "Topic :: Utilities",
    ])
