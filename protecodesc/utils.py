# Copyright (c) 2017, Synopsys, Inc. All rights reserved.
# License: MIT
"""Protecode SC command line tool utilities."""

from __future__ import absolute_import, division, print_function

import hashlib
import logging
import os
import os.path

import requests

logger = logging.getLogger(__name__)


class TimeoutHTTPAdapter(requests.adapters.HTTPAdapter):
    """HTTP Adapter with timeout support.

    This is used so that every request doesn't have to explicitly set
    timeout value.
    """

    # Default timeout
    timeout = 10

    def send(self, request, timeout=None, **kwargs):
        """Call base class send with default timeout."""
        if timeout is None:
            timeout = self.timeout
        return super(TimeoutHTTPAdapter, self).send(
            request, timeout=timeout, **kwargs)


def file_sha1(fname):
    """Compute SHA1 for file."""
    digest = hashlib.sha1()
    with open(fname, 'rb') as filed:
        while True:
            block = filed.read(2**10)  # 1kB
            if not block:
                break
            digest.update(block)
    return digest.hexdigest()


def clean_version(version_text):
    """Clean version string."""
    return version_text.strip().split(' ')[0]


def zip_directory(path, zip_file):
    """Zip directory contents recursively.

    with zipfile.ZipFile('foo.zip', 'w') as zip_file:
        zip_directory('src/directory/', zip_file)
    """
    for root, _dirs, files in os.walk(path):
        for file in files:
            file_path = os.path.join(root, file)
            file_is_file = os.path.isfile(file_path)
            file_is_link = os.path.islink(file_path)
            if file_is_file and not file_is_link:
                zip_file.write(file_path)
            else:
                logger.debug('Ignored non-file %s', file_path)
