# Copyright (c) 2017, Synopsys, Inc. All rights reserved.
# License: MIT
"""Protecode SC command line tool."""

__version__ = '0.2.0'
