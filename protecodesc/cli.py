# Copyright (c) 2017, Synopsys, Inc. All rights reserved.
# License: MIT
"""Protecode SC command line tool."""

from __future__ import absolute_import, division, print_function

import functools
import json
import logging
import os.path
import sys
import time

from tempfile import NamedTemporaryFile
from zipfile import ZipFile

import click

from protecodesc import exceptions
from protecodesc.config import ClientConfig
from protecodesc.protecodesc import ProtecodeSC
from protecodesc.utils import clean_version, zip_directory


logger = logging.getLogger(__name__)

VERDICT_PASS_SYMBOL = u"\U0001F60A"  # SMILING FACE WITH SMILING EYES
VERDICT_FAIL_SYMBOL = u"\U0001F622"  # CRYING FACE
VERDICT_VERIFY_SYMBOL = u"\U0001f440"  # EYES

# Default to Protecode SC online service
DEFAULT_PROTECODESC_HOST = 'https://protecode-sc.com'


def get_protecodesc(insecure=False):
    """Configure and return Protecode SC instance."""
    config = ClientConfig()
    username, password = config.credentials()
    if not (username and password):
        click.echo("Login required.")
        username, password = update_login_credentials()
    # Support alternate Protecode SC address, e.g. appliance.
    protecodesc_host = config.get_host() or DEFAULT_PROTECODESC_HOST
    protecodesc = ProtecodeSC(
        creds=(username, password), host=protecodesc_host, insecure=insecure)
    return protecodesc


def use_protecodesc(func):
    """Initialize Protecode SC instance."""
    @click.option(
        '--insecure/--verify-ssl',
        help="Do not verify TLS certificate for HTTPS")
    @functools.wraps(func)
    def inner(insecure, **kwargs):
        if insecure:
            # If user chose to use insecure explicitly, ignore warnings...
            try:
                import requests.packages.urllib3 as urllib3
                urllib3.disable_warnings()
                click.echo("Warning: Not verifying TLS certificates.")
            except ImportError:
                pass  # If requests moves urllib3 around
        protecodesc = get_protecodesc(insecure=insecure)
        func(protecodesc, **kwargs)
    return inner


@click.group(help="Protecode SC command line tools. To use this tool you need "
                  "to have an account on the service.")
def cli():
    """Protecode SC command line utility."""


@cli.add_command
@click.command()
@use_protecodesc
def groups(protecodesc):
    """List groups."""
    res = protecodesc.list_groups()
    click.echo('Available groups')
    click.echo('{id:8s} {name}'.format(id='ID', name='Name'))
    for grp in res['groups']:
        click.echo('{id:<8d} {name}'.format(**grp))


@cli.add_command
@click.option('--group', help="Show applications in GROUP", metavar="GROUP")
@click.command()
@use_protecodesc
def list(protecodesc, group):
    """List apps."""
    apps = protecodesc.list_apps(group=group)

    app_format = u"{id:5s}  {name}"
    if apps['products']:
        click.echo(app_format.format(id="ID", name="Application name"))
        for prod in apps['products']:
            click.echo(app_format.format(id=str(prod['product_id']),
                                         name=prod['name']))
    else:
        click.echo("No apps found.")


@cli.add_command
@click.argument('default_group', 'Set default group')
@click.command()
def group(default_group):
    """Set default group."""
    config = ClientConfig()
    config.set_default_group(default_group)


@cli.add_command
@click.argument('id_or_sha1', 'Analysis ID or file SHA1 hash')
@click.option('json_output', '--json/--human', default=False,
              help='Output in machine-readable JSON or human')
@click.command()
@use_protecodesc
def result(protecodesc, id_or_sha1, json_output):
    """Get scan result."""
    _print_result(protecodesc, id_or_sha1=id_or_sha1, json_output=json_output)


@cli.add_command
@click.argument('id_or_sha1', 'Analysis ID or file SHA1 hash')
@click.option(
    '--background/--wait',
    help="Scan in background; default: wait for results", default=False)
@click.command()
@use_protecodesc
def rescan(protecodesc, id_or_sha1, background):
    """Request rescan of existing product."""
    click.echo(
        "Requested rescan of {id_or_sha1}".format(id_or_sha1=id_or_sha1))
    try:
        protecodesc.rescan(id_or_sha1)
    except exceptions.ResultNotFound:
        click.echo("Product was not found")
        return
    if not background:
        _print_result(
            protecodesc, id_or_sha1=id_or_sha1, json_output=False, wait=True)


@cli.add_command
@click.argument('id_or_sha1', 'Analysis ID or file SHA1 hash')
@click.command()
@use_protecodesc
def remove(protecodesc, id_or_sha1):
    """Delete the binary file, keep analysis result."""
    click.echo(
        "Requested remove of {id_or_sha1} binary file".format(
            id_or_sha1=id_or_sha1))
    try:
        protecodesc.remove(id_or_sha1)
    except exceptions.ResultNotFound:
        click.echo("Product was not found")
        return


def _print_result(protecodesc, id_or_sha1, json_output, wait=True):
    wait_printed = False
    while True:
        try:
            data = protecodesc.get_result(id_or_sha1=id_or_sha1)
            res = data.get('results', {})
            if wait and res['status'] == ProtecodeSC.STATUS_BUSY:
                if not wait_printed:
                    wait_printed = True
                    click.echo("Waiting for result for {id_or_sha1}"
                               .format(id_or_sha1=id_or_sha1))
                time.sleep(5)
                continue
            break
        except exceptions.ResultNotFound:
            click.echo("Result not found")
            return

    if json_output:
        click.echo(json.dumps(data))
        return

    summary = res['summary']
    filename = res.get('filename', "")
    sha1 = res.get('sha1sum')
    components = res.get('components', [])
    report_url = res.get('report_url')

    # Component analysis
    component_texts = set()
    for comp in components:
        c_lib = comp.get('lib')
        c_version = comp.get('version')
        if c_version:
            c_version = clean_version(c_version)
            c_text = "{lib} ({version})".format(lib=c_lib, version=c_version)
        else:
            c_text = "{lib}".format(lib=c_lib)
        component_texts.add(c_text)
    # Number of vulnerable components
    vuln_components = sum([1 for comp in components if comp['vulns']])
    total_components = len(components)

    # License analysis
    lic_unknown = {'name': 'UNKNOWN'}
    licenses = [x.get('license', lic_unknown)['name'] for x in components]

    # Print output
    click.echo("Analysis results")
    click.echo("    File:   {name}".format(name=filename))
    click.echo("    SHA1:   {sha1}".format(sha1=sha1))
    if report_url:
        click.echo("    Report: {uri}".format(uri=report_url))

    if not res['status'] == ProtecodeSC.STATUS_READY:
        click.echo("Result not yet ready.")
        return

    if component_texts:
        click.echo()
        click.echo('Components:')
        click.echo('    ' + ' '.join(sorted(component_texts)))
    else:
        click.echo("No 3rd party or open source components detected.")

    if licenses:
        click.echo()
        click.echo('License analysis:')
        click.echo('    ' + ' '.join(sorted(set(licenses))))

    click.echo()
    verdict = summary['verdict']['short']
    if verdict == 'Verify':
        symbol = VERDICT_VERIFY_SYMBOL
    elif verdict == 'Vulns':
        symbol = VERDICT_FAIL_SYMBOL
    elif verdict == 'Pass':
        symbol = VERDICT_PASS_SYMBOL
    else:
        symbol = '??'
    click.echo('Vulnerability analysis:')
    click.echo(u'    {vuln} out of {total} components contain known '
               u'vulnerabilities {sym}'.format(vuln=vuln_components,
                                               total=total_components,
                                               sym=symbol))
    click.echo(u'    ' + summary['verdict']['detailed'])


@cli.add_command
@click.argument(
    'file', 'file to analyze', nargs=-1, required=True,
    type=click.Path(exists=True))
@click.option(
    '--group', help="Upload to group id GROUP (see group)", metavar="GROUP",
    type=int)
@click.option(
    '--background/--wait',
    help="Scan in background; default: wait for results", default=False)
@click.command()
@use_protecodesc
def scan(protecodesc, file, group, background):
    """Analyze a file or directory.

    If a directory is analyzed, it will be compressed to a ZIP archive before
    upload.
    """
    if not group:
        group = ClientConfig().get_default_group()

    file_count = len(file)
    click.echo('Uploading {count} objects...'.format(count=file_count))
    upload_shasums = []
    for _i, file_path in enumerate(file):
        display_name = click.format_filename(file_path)
        click.echo(display_name)

        if os.path.isdir(file_path):
            # Upload directory as ZIP
            logger.info("Zipping directory...")
            zip_name = "{dirname}.zip".format(
                dirname=os.path.basename(display_name.rstrip(os.path.sep)))
            with NamedTemporaryFile() as tmp_file:
                with ZipFile(tmp_file.name, 'w', allowZip64=True) as zip_file:
                    zip_directory(file_path, zip_file)
                res = protecodesc.upload_file(
                    tmp_file.name, display_name=zip_name, group=group)
        else:
            # Regular file, upload as is
            res = protecodesc.upload_file(file_path, group=group)

        res = res.get('results', res)
        if res.get('status') == ProtecodeSC.STATUS_READY:
            status = 'READY; scanned before'
        else:
            status = 'queued for scanning'
        report_url = res.get('report_url')
        sha1_checksum = res.get('sha1sum')
        upload_shasums.append(sha1_checksum)
        click.echo(" - SHA1: {sha1}".format(sha1=sha1_checksum))
        click.echo(" - {url} ({status})".format(url=report_url,
                                                status=status))

    if not background:
        click.echo()
        click.echo("="*50)
        for sha1 in upload_shasums:
            _print_result(protecodesc, id_or_sha1=sha1, json_output=False,
                          wait=True)
            click.echo("="*50)


@cli.add_command
@click.argument('id_or_sha1', 'Analysis ID or file SHA1 hash')
@click.command()
@use_protecodesc
def delete(protecodesc, id_or_sha1):
    """Delete product along with results"""
    click.confirm(
        'Really delete all data for product?'.format(id=id_or_sha1), abort=True)
    try:
        protecodesc.delete(id_or_sha1)
    except exceptions.ResultNotFound:
        click.echo("Product was not found")
        return


@cli.add_command
@click.command()
def login():
    """Save username/password and configure server address."""
    config = ClientConfig()
    if click.confirm(
            "Use Protecode SC managed service https://protecode-sc.com/?"):
        config.set_host(DEFAULT_PROTECODESC_HOST)
    else:
        host = click.prompt("Enter URI (https://YOUR-APPLIANCE)")
        config.set_host(host)
    update_login_credentials()


@cli.add_command
@click.command()
def logout():
    """Forget saved username and password."""
    config = ClientConfig()
    config.forget_credentials()


def update_login_credentials():
    """Update login credentials."""
    username = click.prompt("Login username/email-address")
    password = click.prompt("Login password", hide_input=True)
    config = ClientConfig()
    if click.confirm('Save information and do not ask again?'):
        config.set_credentials(username, password)
        click.echo("Saved login details.")
    return username, password


def main(retries=2):
    """Run command line tool."""
    # create logger
    logger = logging.getLogger('protecodesc')
    logger.setLevel(logging.WARNING)

    # create console handler and set level to debug
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.DEBUG)

    # create formatter
    formatter = logging.Formatter('%(levelname)s: %(message)s')

    # add formatter to console handler
    console_handler.setFormatter(formatter)

    # add console handler to logger
    logger.addHandler(console_handler)

    try:
        for _i in range(0, retries):
            try:
                cli(obj={})
            except exceptions.InvalidLoginError:
                click.echo("Login failed. Please log in again.")
                update_login_credentials()
        click.echo("Out of retries, aborting.")
    except KeyboardInterrupt:
        pass
    except exceptions.ConnectionFailure as error:
        click.echo(error)
        sys.exit(1)


if __name__ == '__main__':
    main()
