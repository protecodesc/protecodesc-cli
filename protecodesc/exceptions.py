# Copyright (c) 2017, Synopsys, Inc. All rights reserved.
# License: MIT
"""Protecode SC command line tool exceptions."""


class ProtecodeSCException(Exception):
    """Protecode SC interface exception."""


class ConnectionFailure(ProtecodeSCException):
    """Connection to API failed."""


class OutOfRetriesError(ConnectionFailure):
    """Ran out of retries with a HTTP request."""


class ResultNotFound(ProtecodeSCException):
    """Result for requested ID or SHA1 was not found."""


class InvalidLoginError(ProtecodeSCException):
    """Login was rejected."""
